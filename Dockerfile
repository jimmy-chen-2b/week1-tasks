FROM ruby:2.7.5
RUN apt-get update -yqq
RUN apt-get install -yqq --no-install-recommends git nodejs npm
RUN npm install -g yarn
RUN gem install rails

WORKDIR /projects/week1-task
COPY Gemfile* /projects/week1-task/
RUN bundle install
COPY . /projects/week1-task/

CMD [ "bin/rails", "s", "-b", "0.0.0.0" ]